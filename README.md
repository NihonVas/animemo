# Animemo

A jikan-based anime library

## MVP-1
### Multiple types of searches:
By tag, by season, by score, by genre, etc..
### Seasonal charts
### Daily quizes

## Execution
```
# Running in a dev mode
npm start

# Build
npm run build:prod
```

## Authors
Anatoliy Baskakov
Evgeniy Lutanin
Lev Lymarenko