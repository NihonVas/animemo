const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        'animemo.main': '/animemo',
        'link.animemo.auth': '/auth'
    },
    features: {
        'animemo': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'animemo.api': '/api',
    }
}
